## Clean code practices while writing code in javascript

1. Avoid using shortcuts or short names while giving name to the variables or functions. Instead we can give the name which describe the purpose or function while watching that name itself.

            let name = "Javascript"

            //Instead of giving just name, if we give course_name it makes some meaning right regarding name is  related to the course name.

            let courseName = "Javascript"

2. While passing arguments as parameters to the functions better to avoid the boolean values.

            function greetToUser(isLogin) {
                if (isLogin) {
                    console.log("Good Morning!")
                } else {
                    console.log("LOGIN HERE")
                }
            }

            let isLogin = true
            greetToUser(isLogin)

            // Instead of writing condition inside the function we can write two functions like this


            function messageToLoggedInUser() {
                console.log("Good Morning!")
            }

            function messageToNotLoggedInUser() {
                console.log("LOGIN HERE")
            }

            if (isLogin) {
                messageToLoggedInUser()
            } else {
                messageToNotLoggedInUser()
            }

            

3. Better to add prefix "is" at the begining of the variable name if we are going to assign a boolean value to it.

            let login = true 
            // Instead of using login as variable name if we use isLogin, it describe the meaning of that varialble name like for what purpose we are using.
            let isLogin = true

4. While dealing with the events, its better to give name which describe that event.

            <input type="text" onChange = {this.inputEvent} value={username} placeholder="Enter username" />

            //Instead of using the inputEvent to the onChange event if we use onChangeUsername, it will show some meaning whats going with this event and we can easily identifies what this indicates

            <input type="text" onChange = {this.onChangeUsername} value={username} placeholder="Enter username" />


5. Try to write code in few lines instead of writing in so many lines.

            let (isLogin === true) {
                console.log("Good Morning!")
            } esle {
                console.log("LOGIN HERE")
            }

            //Instead of writing in 5 lines we can write in single line using terinary operator

            isLogin ? console.log("Good Morning") : console.log("LOGIN HERE")
